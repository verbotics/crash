# Compute the similarity in meaning between two given sentences.

import spacy
import sys

nlp = spacy.load('en_core_web_sm')                          #'en_core_web_lg' for SpaCy word vector.
                                                            #'en_core_web_sm' for SpaCy similarity.


# getSimilarity returns a float between 0 to 1, corresponding to the semantic similarity between the two sentences.
def getSimilarity(in1='\0', in2='\0'):
    # Function was called with no arguments.
    if (in1=='\0' and in2=='\0'):
        in1 = input('Enter first sentence:\n')
        in2 = input('Enter second sentence:\n')
    # Function was called with only one argument.
    elif (in1=='\0' or in2=='\0'):
        in2 = input('Enter second sentence:\n')
    # Compute and print similarity.    
    doc1 = nlp(in1)
    doc2 = nlp(in2)
    similarity = doc1.similarity(doc2)
    print("Similarity: ", similarity)
    
    
def main():
    getSimilarity("Bob crashed into a tree.", "I am a human.")
    #getSimilarity("Bob crashed into a tree.")
    #getSimilarity()
     
main()