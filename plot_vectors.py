# This should take as input an entire document, and return the 2(currently 2 but may change) most similar sentences in meaning.



# Possible approaches to this problem:    => Separate the document into an array containing sentences. (the length n of the array will be the total no. of senteces found in the entire text.)
#                                            - Find out how to go through a text and identify where a sentence starts and ends.
#                                         => Compare each pair of elements in the array and get their similarity score.
#                                            - Need to cycle through every (i,j) where i->n and j->n and i=/=j.
#                                         => Create n*n matrix of scores, and choose to return the best one.
#


# Takes in a document, and outputs an array where each element is a sentence from the document.
import requests
import spacy


# Given a file_path, genSentenceArr will load the document and turn it into an array of sentences.
# Example of file_path ->  'C:/Users/Dennis/BitBucketRepos/VerboticsCrash/crash/MachineLearningWiki.txt'

import requests
import spacy
import matplotlib.pyplot as plt
import statistics as stats
import numpy as np
nlp = spacy.load('en_core_web_lg')

#'C:/Users/Dennis/BitBucketRepos/VerboticsCrash/crash/MachineLearningWiki.txt'
# Given a file_path, genSentenceArr will load the document and turn it into an array of spans(sentences of word vectors).


def gen_sentence_arr(file_path):
    f = open(file_path, 'r', encoding='utf-8')
    document = f.read()
    f.close()
    nlp.add_pipe(nlp.create_pipe('sentencizer'))
    doc = nlp(document)
    span_arr = list(doc.sents)
    return list(map(lambda x: x.text, span_arr))


# Get Vectors of each word given an input sentence. Returns a Array of Vectors (represents the sentence).
def gen_vector(sentence):
    tokens = nlp(sentence)
    return list(map(float, map(lambda x: x.vector_norm, tokens)))


def plot_vectors(vectors):
    x, y = zip(*vectors)
    ax = plt.gca()
    ax.set_xlim([0, 8])
    ax.set_ylim([0, 1])
    for i in range(len(vectors)):
        plt.quiver(0, 0, x[i], y[i], angles='xy', scale_units='xy', scale=1)
        plt.text(x[i]+0.02, y[i], i, fontsize=12)
    plt.xlabel('mean')
    plt.ylabel('std_dev')
    plt.show()


def gen_means_and_stddev(sentences):
    word_vectors = [gen_vector(sentence) for sentence in sentences]
    vectors = [(stats.mean(vec), stats.stdev(vec) if len(vec) > 1 else vec[0])
               for vec in word_vectors]
    plot_vectors(vectors)


sentence_list = ["Hi",
                 "hi",
                 "Hello",
                 "hello",
                 "Hello, how are you?",
                 "Howdy, how are you?",
                 "Hi, how are you?",
                 "Good afternoon, how are you?",
                 "How are you doing on this fine evening?",
                 "Hello, how are you feeling?",
                 "Hello, how are you feeling today?",
                 "Howdy, what a lovely day."]

gen_means_and_stddev(sentence_list)
