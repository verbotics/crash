# This should take as input an entire document, and return the 2(currently 2 but may change) most similar sentences in meaning.



# Possible approaches to this problem:    => Separate the document into an array containing sentences. (the length n of the array will be the total no. of senteces found in the entire text.)
#                                            - Find out how to go through a text and identify where a sentence starts and ends.
#                                         => Compare each pair of elements in the array and get their similarity score.
#                                            - Need to cycle through every (i,j) where i->n and j->n and i=/=j.
#                                         => Create n*n matrix of scores, and choose to return the best one.
#


# Takes in a document, and outputs an array where each element is a sentence from the document.
import requests
import spacy


# Given a file_path, genSentenceArr will load the document and turn it into an array of sentences.
# Example of file_path ->  'C:/Users/Dennis/BitBucketRepos/VerboticsCrash/crash/MachineLearningWiki.txt'

import requests
import spacy
nlp = spacy.load('en_core_web_lg')

#'C:/Users/Dennis/BitBucketRepos/VerboticsCrash/crash/MachineLearningWiki.txt'
# Given a file_path, genSentenceArr will load the document and turn it into an array of spans(sentences of word vectors).
def genSentenceArr(file_path):
    f = open(file_path, 'r', encoding='utf-8')
    document = f.read()
    f.close()
    nlp.add_pipe(nlp.create_pipe('sentencizer'))
    doc = nlp(document)
    spanArr = list(doc.sents)
    return list(map(lambda x:x.text, spanArr))

# Get Vectors of each word given an input sentence. Returns a Array of Vectors (represents the sentence).
def genVector(sentence):
    tokens = nlp(sentence)
    return list(map(lambda x:x.vector_norm, tokens))